using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Remoting.Metadata.W3cXsd2001;


namespace Core
{
    public class CommonUtils
    {
        

		/* TODO: not readily available on monotouch

		[DllImport("msvcrt.dll", CallingConvention=CallingConvention.Cdecl)]
		static extern int memcmp(byte[] b1, byte[] b2, long count);

		public static bool ByteArrayCompare(byte[] b1, byte[] b2)
		{
			// Validate buffers are the same length.
			// This also ensures that the count does not exceed the length of either buffer.
			if (b1 == null || b2 == null)
				return b1 == b2;
			return b1.Length == b2.Length && memcmp(b1, b2, b1.Length) == 0;
		}*/

		// An alternative to ByteArrayCompare if msvcrt.dll is not available somewhere.
		// See: http://stackoverflow.com/questions/43289/comparing-two-byte-arrays-in-net
#if !SAFE
        public static unsafe bool ByteArraysEqual(byte[] a1, byte[] a2) {
            if (a1 == a2)
                return true;
			if(a1==null || a2==null || a1.Length!=a2.Length)
				return false;
			fixed (byte* p1=a1, p2=a2) {
				byte* x1=p1, x2=p2;
				int l = a1.Length;
				for (int i=0; i < l/8; i++, x1+=8, x2+=8)
					if (*((long*)x1) != *((long*)x2)) return false;
				if ((l & 4)!=0) { if (*((int*)x1)!=*((int*)x2)) return false; x1+=4; x2+=4; }
				if ((l & 2)!=0) { if (*((short*)x1)!=*((short*)x2)) return false; x1+=2; x2+=2; }
				if ((l & 1)!=0) if (*((byte*)x1) != *((byte*)x2)) return false;
				return true;
			}
		}
#else
        public static bool ByteArraysEqual(byte[] a1, byte[] a2)
        {
            if (a1 == null || a2 == null || a1.Length != a2.Length)
                return false;
            for(var i = 0; i < a1.Length; ++i)
                if (a1[i] != a2[i])
                    return false;
            return true;
        }
#endif

    public class ByteArrayComparer : IEqualityComparer<byte[]> {
		public bool Equals(byte[] left, byte[] right) {
            return CommonUtils.ByteArraysEqual (left, right);
		}

		public int GetHashCode(byte[] array)
		{
			return ByteArrayComparer.GetByteArrayHashCode (array);
		}

		public static int GetByteArrayHashCode(byte[] array)
		{
			unchecked {
				int i = 0;
				int hash = 17;
				int rounded = array.Length & ~3;

				hash = 31 * hash + array.Length;

				for (; i < rounded; i += 4) {
					hash = 31 * hash + BitConverter.ToInt32 (array, i);
				}

				if (i < array.Length) {
					int val = array [i];
					i++;

					if (i < array.Length) {
						val |= array [i] << 8;
						i++;

						if (i < array.Length) {
							val |= array [i] << 16;
						}
					}

					hash = 31 * hash + val;
				}
				return hash;
			}
		}
	}
    }
}