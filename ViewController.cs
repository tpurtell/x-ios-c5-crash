﻿using System;

using UIKit;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace c5crash
{
    public partial class ViewController : UIViewController
    {
        public ViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            // Perform any additional setup after loading the view, typically from a nib.
            BuildList();
            for(var i = 0; i < 10; ++i) {
                Task.Run(() => WorkOnSet());
            }
        }

        public override void DidReceiveMemoryWarning ()
        {
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
        }

        object _Lock = new object();
        C5.HashedLinkedList<byte[]> _Set = new C5.HashedLinkedList<byte[]>(new Core.CommonUtils.ByteArrayComparer());
        List<byte[]> _Candidates = new List<byte[]>();

        //reususe a small list of entries so that we exercise more paths of the hll
        void BuildList() {
            var r = new Random();
            for(var i = 0; i < 20; ++i) {
                var key = new byte[16];
                r.NextBytes(key);
                _Candidates.Add(key);
            }
        }

        async Task WorkOnSet() {
            var r = new Random();
            for(;;) {
                var i = r.Next(_Candidates.Count);
                var key = _Candidates[i];
                key = (byte[])key.Clone();

                lock(_Lock) {
                    if(r.Next(2) != 0)
                        _Set.Add(key);
                    else

                        _Set.Remove(key);
                }
            }
        }
    }
}

